# This file starts hadoop processes and should be run by the user under which
# the hadoop process should be run

if [ "x$CLASSPATH_CONFIG" = "x" ]
then
  CLASSPATH_CONFIG=/etc/hadoop/conf
fi

if [ "x$CLASSPATH_CORE" = "x" ]
then
    JARS="hadoop-core.jar
hadoop-metrics.jar
hadoop-streaming.jar
commons-cli.jar
commons-codec.jar
commons-el.jar
commons-httpclient.jar
commons-io.jar
commons-logging-api.jar
commons-logging.jar
commons-logging-adapters.jar
commons-net.jar
servlet-api-2.5.jar
tomcat-juli.jar
xmlenc.jar
jetty.jar
jetty-util.jar
log4j-1.2.jar
hsqldb.jar
jasper.jar
jsp-api-2.1.jar
el-api-2.1.jar
jasper-el.jar"

  for JAR in $JARS
  do
    CLASSPATH_CORE=${CLASSPATH_CORE}:/usr/share/java/${JAR}
  done
  
  # Jetty needs the webapps root dir to be in the classpath
  # There's one additional : at the beginning of ${CLASSPATH_CORE}
  CLASSPATH_CORE=/usr/share/hadoop${CLASSPATH_CORE}
fi

for f in /usr/share/java/hadoop*tools*.jar; do
  TOOL_PATH=${TOOL_PATH}:$f;
done

# figure out which class to run
if [ "$COMMAND" = "namenode" ] ; then
  CLASS='org.apache.hadoop.hdfs.server.namenode.NameNode'
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_NAMENODE_OPTS"
elif [ "$COMMAND" = "secondarynamenode" ] ; then
  CLASS='org.apache.hadoop.hdfs.server.namenode.SecondaryNameNode'
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_SECONDARYNAMENODE_OPTS"
elif [ "$COMMAND" = "datanode" ] ; then
  CLASS='org.apache.hadoop.hdfs.server.datanode.DataNode'
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_DATANODE_OPTS"
elif [ "$COMMAND" = "fs" ] ; then
  CLASS=org.apache.hadoop.fs.FsShell
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "dfs" ] ; then
  CLASS=org.apache.hadoop.fs.FsShell
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "dfsadmin" ] ; then
  CLASS=org.apache.hadoop.hdfs.tools.DFSAdmin
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "mradmin" ] ; then
  CLASS=org.apache.hadoop.mapred.tools.MRAdmin
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "fsck" ] ; then
  CLASS=org.apache.hadoop.hdfs.tools.DFSck
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "balancer" ] ; then
  CLASS=org.apache.hadoop.hdfs.server.balancer.Balancer
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_BALANCER_OPTS"
elif [ "$COMMAND" = "jobtracker" ] ; then
  CLASS=org.apache.hadoop.mapred.JobTracker
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_JOBTRACKER_OPTS"
elif [ "$COMMAND" = "tasktracker" ] ; then
  CLASS=org.apache.hadoop.mapred.TaskTracker
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_TASKTRACKER_OPTS"
elif [ "$COMMAND" = "job" ] ; then
  CLASS=org.apache.hadoop.mapred.JobClient
elif [ "$COMMAND" = "queue" ] ; then
  CLASS=org.apache.hadoop.mapred.JobQueueClient
elif [ "$COMMAND" = "pipes" ] ; then
  CLASS=org.apache.hadoop.mapred.pipes.Submitter
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "version" ] ; then
  CLASS=org.apache.hadoop.util.VersionInfo
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "jar" ] ; then
  CLASS=org.apache.hadoop.util.RunJar
elif [ "$COMMAND" = "distcp" ] ; then
  CLASS=org.apache.hadoop.tools.DistCp
  CLASSPATH=${CLASSPATH}:${TOOL_PATH}
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "daemonlog" ] ; then
  CLASS=org.apache.hadoop.log.LogLevel
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "archive" ] ; then
  CLASS=org.apache.hadoop.tools.HadoopArchives
  CLASSPATH=${CLASSPATH}:${TOOL_PATH}
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
elif [ "$COMMAND" = "sampler" ] ; then
  CLASS=org.apache.hadoop.mapred.lib.InputSampler
  HADOOP_OPTS="$HADOOP_OPTS $HADOOP_CLIENT_OPTS"
else
  CLASS=$COMMAND
fi

# Where log files are stored.
if [ "x$HADOOP_LOG_DIR" = "x" ]
then
  if [ -w /var/log/hadoop ]
  then
    HADOOP_LOG_DIR=/var/log/hadoop
  else
    HADOOP_LOG_DIR=${HOME}/.hadoop/log
    mkdir -p $HADOOP_LOG_DIR
  fi
fi

if [ "x$HADOOP_IDENT_STRING" = "x" ]
then
  HADOOP_IDENT_STRING=$USER
fi

if [ "x$HOSTNAME" = "x" ]
then
  HOSTNAME=$(hostname)
fi

if [ "x$HADOOP_LOGFILE" = "x" ]
then
  HADOOP_LOGFILE=$HADOOP_IDENT_STRING-$COMMAND-$HOSTNAME.log
fi

if [ "x$HADOOP_HOME" = "x" ]
then
  HADOOP_HOME=/usr/share/hadoop
fi

if [ "x$HADOOP_ROOT_LOGGER" = "x" ]
then
  HADOOP_ROOT_LOGGER="INFO,DRFA"
fi

if [ "x$HADOOP_ROOT_LOGGER" = "x" ]
then
  # TODO What does DRFA mean, and isn't INFO,console the default?
  HADOOP_ROOT_LOGGER="INFO,DRFA"
fi

# default policy file for service-level authorization
if [ "x$HADOOP_POLICYFILE" = "x" ]
then
  HADOOP_POLICYFILE="hadoop-policy.xml"
fi

HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.log.dir=$HADOOP_LOG_DIR"
HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.log.file=$HADOOP_LOGFILE"
HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.home.dir=$HADOOP_HOME"
HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.id.str=$HADOOP_IDENT_STRING"
HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.root.logger=${HADOOP_ROOT_LOGGER}"
if [ "x$JAVA_LIBRARY_PATH" != "x" ]
then
  HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$JAVA_LIBRARY_PATH"
fi  
HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.policy.file=$HADOOP_POLICYFILE"

exec "$JAVA" $JAVA_HEAP_MAX $HADOOP_OPTS -classpath "$CLASSPATH":"$CLASSPATH_CONFIG":"$CLASSPATH_CORE" $CLASS "$@"
