.\" Man page generated from reStructeredText.
.
.TH HADOOP 1 "2010-02-01" "0.2" "cluster computing"
.SH NAME
hadoop \- software platform to process vast amounts of data
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.SH SYNOPSIS
.sp
Usage: hadoop [\-\-config confdir] COMMAND
.SH DESCRIPTION
.sp
Here’s what makes Hadoop especially useful:
.INDENT 0.0
.TP
.B Scalable
.
Hadoop can reliably store and process petabytes.
.TP
.B Economical
.
It distributes the data and processing across clusters of commonly
available computers. These clusters can number into the thousands
of nodes.
.TP
.B Efficient
.
By distributing the data, Hadoop can process it in parallel on the
nodes where the data is located. This makes it extremely rapid.
.TP
.B Reliable
.
Hadoop automatically maintains multiple copies of data and automat‐
ically redeploys computing tasks based on failures.
.UNINDENT
.sp
Hadoop implements MapReduce, using the Hadoop Distributed File System
(HDFS) (see figure below.) MapReduce divides applications into many
small blocks of work. HDFS creates multiple replicas of data blocks for
reliability, placing them on compute nodes around the cluster. MapRe‐
duce can then process the data where it is located.
.sp
For more details about hadoop, see the Hadoop Wiki at
\fI\%http://wiki.apache.org/hadoop/\fP.
.SH OPTIONS
.INDENT 0.0
.TP
.BI \-\-config \ configdir
.
Overrides the "HADOOP_CONF_DIR" environment variable.  See "ENVI‐
RONMENT" section below.
.UNINDENT
.SH COMMANDS
.INDENT 0.0
.TP
.B namenode \-format
.
format the DFS filesystem
.TP
.B secondarynamenode
.
run the DFS secondary namenode
.TP
.B namenode
.
run the DFS namenode
.TP
.B datanode
.
run a DFS datanode
.TP
.B dfsadmin
.
run a DFS admin client
.TP
.B fsck
.
run a DFS filesystem checking utility
.TP
.B fs
.
run a generic filesystem user client
.TP
.B balancer
.
run a cluster balancing utility
.TP
.B jobtracker
.
run the MapReduce job Tracker node
.TP
.B pipes
.
run a Pipes job
.TP
.B tasktracker
.
run a MapReduce task Tracker node
.TP
.B job
.
manipulate MapReduce jobs
.TP
.B version
.
print the version
.TP
.B jar <jar>
.
run a jar file
.TP
.B distcp <srcurl> <desturl>
.
copy file or directories recursively
.TP
.B archive \-archiveName NAME <src>* <dest>
.
create a hadoop archive
.TP
.B daemonlog
.
get/set the log level for each daemon
.TP
.B CLASSNAME
.
run the class named CLASSNAME
.UNINDENT
.sp
Most commands print help when invoked w/o parameters.
.SH FILESYSTEM COMMANDS
.sp
The following commands can be used with the fs command like
hadoop fs [filesystem command]
.INDENT 0.0
.IP \(bu 2
.
\-ls <path>
.IP \(bu 2
.
\-lsr <path>
.IP \(bu 2
.
\-du <path>
.IP \(bu 2
.
\-dus <path>
.IP \(bu 2
.
\-count[\-q] <path>
.IP \(bu 2
.
\-mv <src> <dst>
.IP \(bu 2
.
\-cp <src> <dst>
.IP \(bu 2
.
\-rm [\-skipTrash] <path>
.IP \(bu 2
.
\-rmr [\-skipTrash] <path>
.IP \(bu 2
.
\-expunge
.IP \(bu 2
.
\-put <localsrc> ... <dst>
.IP \(bu 2
.
\-copyFromLocal <localsrc> ... <dst>
.IP \(bu 2
.
\-moveFromLocal <localsrc> ... <dst>
.IP \(bu 2
.
\-get [\-ignoreCrc] [\-crc] <src> <localdst>
.IP \(bu 2
.
\-getmerge <src> <localdst> [addnl]
.IP \(bu 2
.
\-cat <src>
.IP \(bu 2
.
\-text <src>
.IP \(bu 2
.
\-copyToLocal [\-ignoreCrc] [\-crc] <src> <localdst>
.IP \(bu 2
.
\-moveToLocal [\-crc] <src> <localdst>
.IP \(bu 2
.
\-mkdir <path>
.IP \(bu 2
.
\-setrep [\-R] [\-w] <rep> <path/file>
.IP \(bu 2
.
\-touchz <path>
.IP \(bu 2
.
\-test \-[ezd] <path>
.IP \(bu 2
.
\-text <src>
.IP \(bu 2
.
\-copyToLocal [\-ignoreCrc] [\-crc] <src> <localdst>
.IP \(bu 2
.
\-moveToLocal [\-crc] <src> <localdst>
.IP \(bu 2
.
\-mkdir <path>
.IP \(bu 2
.
\-setrep [\-R] [\-w] <rep> <path/file>
.IP \(bu 2
.
\-touchz <path>
.IP \(bu 2
.
\-test \-[ezd] <path>
.IP \(bu 2
.
\-stat [format] <path>
.IP \(bu 2
.
\-tail [\-f] <file>
.IP \(bu 2
.
\-chmod [\-R] <MODE[,MODE]... | OCTALMODE> PATH...
.IP \(bu 2
.
\-chown [\-R] [OWNER][:[GROUP]] PATH...
.IP \(bu 2
.
\-chgrp [\-R] GROUP PATH...
.IP \(bu 2
.
\-help [cmd]
.UNINDENT
.sp
Generic options supported are
.INDENT 0.0
.TP
.B \-conf <configuration file>
.
specify an application configuration file
.UNINDENT
.INDENT 0.0
.TP
.BI \-D \ <property=value>
.
use value for given property
.UNINDENT
.INDENT 0.0
.TP
.B \-fs <local|namenode:port>
.
specify a namenode
.TP
.B \-jt <local|jobtracker:port>
.
specify a job tracker
.TP
.B \-files <comma separated list of files>
.
specify comma separated files to be copied to the map reduce cluster
.TP
.B \-libjars <comma separated list of jars>
.
specify comma separated jar files to include in the classpath.
.TP
.B \-archives <comma separated list of archives>
.
specify comma separated archives to be unarchived on the compute machines.
.UNINDENT
.SH FILES
.INDENT 0.0
.TP
.B /etc/hadoop/conf
.
This symbolic link points to the currently active Hadoop configura‐
tion directory.
.UNINDENT
.sp
Note to Hadoop System Admins
.sp
The "/etc/hadoop/conf" link is managed by the alterna‐
tives(8) command so you should not change this symlink
directly.
.sp
To see what current alternative(8) Hadoop configurations
you have, run the following command:
.sp
.nf
.ft C
# alternatives \-\-display hadoop
hadoop \- status is auto.
 link currently points to /etc/hadoop/conf.pseudo
/etc/hadoop/conf.empty \- priority 10
/etc/hadoop/conf.pseudo \- priority 30
Current \(aqbest\(aq version is /etc/hadoop/conf.pseudo.
.ft P
.fi
.sp
This shows that the link point to "/etc/hadoop/conf.pseudo"
(for the Hadoop Pseudo\-Distributed configuration).
.sp
To add a new custom configuration, run the following com‐
mands as root:
.sp
.nf
.ft C
# cp \-r /etc/hadoop/conf.empty /etc/hadoop/conf.my
.ft P
.fi
.sp
This will create a new configuration directory,
"/etc/hadoop/conf.my", that serves as a starting point for
a new configuration.  Edit the configuration files in
"/etc/hadoop/conf.my" until you have the configuration you
want.
.sp
To activate your new configuration and see the new configu‐
ration list:
.sp
.nf
.ft C
# alternatives \-\-install /etc/hadoop/conf hadoop /etc/hadoop/conf.my 90
.ft P
.fi
.sp
You can verify your new configuration is active by running
the following:
.sp
.nf
.ft C
# alternatives \-\-display hadoop
hadoop \- status is auto.
 link currently points to /etc/hadoop/conf.my
/etc/hadoop/conf.empty \- priority 10
/etc/hadoop/conf.pseudo \- priority 30
/etc/hadoop/conf.my \- priority 90
Current \(aqbest\(aq version is /etc/hadoop/conf.my.
.ft P
.fi
.sp
At this point, it might be a good idea to restart your ser‐
vices with the new configuration, e.g.,
.INDENT 0.0
.INDENT 3.5
.sp
# /etc/init.d/hadoop\-namenode restart
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B /etc/hadoop/conf/hadoop\-site.xml
.
This is the path to the currently deployed Hadoop site configura‐
tion.  See "/etc/hadoop/conf" above.
.TP
.B /usr/bin/hadoop\-config.sh
.
This script searches for a useable "JAVA_HOME" location if
"JAVA_HOME" is not already set.  It also sets up environment vari‐
ables that Hadoop components need at startup (see "ENVIRONMENT"
section).
.TP
.B /etc/init.d/hadoop\-namenode
.
Service script for starting and stopping the Hadoop NameNode
.TP
.B /etc/init.d/hadoop\-datanode
.
Service script for starting and stopping the Hadoop DataNode
.TP
.B /etc/init.d/hadoop\-secondarynamenode
.
Service script for starting and stopping the Hadoop Secondary
NameNode
.TP
.B /etc/init.d/hadoop\-jobtracker
.
Service script for starting and stopping the Hadoop JobTracker
.TP
.B /etc/init.d/hadoop\-tasktracker
.
Service script for starting and stopping the Hadoop TaskTracker
.UNINDENT
.SH ENVIRONMENT
.INDENT 0.0
.TP
.B HADOOP_CONF_DIR
.
The location of the Hadoop configuration files.  Defaults to
"/etc/hadoop/conf".  For more details, see the "FILES" section.
.TP
.B HADOOP_LOG_DIR
.
All Hadoop services log to "/var/log/hadoop" by default.  You can
change the location with this environment variable.
.TP
.B HADOOP_ROOT_LOGGER
.
Setting for log4j. Defaults to ERROR,console. You can try INFO,console for
more verbose output.
.UNINDENT
.SH EXAMPLES
.sp
.nf
.ft C
$ mkdir input
$ cp <txt files> input
$ hadoop jar /usr/lib/hadoop/*example*.jar input output \(aqgrep string\(aq
$ cat output/*
.ft P
.fi
.SH BUGS
.sp
The Debian package of Hadoop is still in beta state. Use it at your own risk!
.SH SEE ALSO
.sp
alternatives(8)
.SH AUTHOR
Cloudera, Thomas Koch <thomas.koch@ymc.ch>
.SH COPYRIGHT
2008 The Apache Software Foundation. All rights reserved.
.\" Generated by docutils manpage writer.
.\" 
.
